benali-youghourta-techtask-201803
---

## How to install

- Clone this repo: `clone https://djug@bitbucket.org/djug/benali-youghourta-techtask-201803.git`
- `cd benali-youghourta-techtask-201803/`
- Install the dependencies: `composer install`
- Lunch the built-in PHP webserver `php artisan serve`
- Visit `http://127.0.0.1:8000/lunch` to see the result

- In order to tweet the result as well, go to `.env` file and update `TWITTER_CONSUMER_KEY`, `TWITTER_CONSUMER_SECRET`, `APP_OAUTH_TOKEN`, and `APP_OAUTH_TOKEN_SECRET` with your own credential (the twitter app and your own tokens).

## Main files/classes
- `/routes/web.php` the entry point where the routes are defined
- `app/Http/Controllers/LunchController.php` the main controller
- `app/Lunch.php` the class that takes care of finding the result
- `app/Twitter.php` the class that sends the tweet
- `app/IngredientsRepository.php` and `app/RecipesRepository.php`: the classes that retrieve the data from the json files. Both classes extend the abstract class `app/JsonFileRepository.php` which does the "actual talking" with the json files. this abstract class implements `app/BasicRepository.php`
- `tests/Unit/LunchTest.php` the unit tests class


