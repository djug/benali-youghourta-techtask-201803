<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\RecipesRepository;
use App\IngredientsRepository;
use App\Lunch;


class ExampleTest extends TestCase
{
    private $recipes;
    private $ingredients;
    private $lunch;

    public function setUp()
    {
        $this->recipes = '{ "recipes": [ { "title": "recipe 01", "ingredients": ["ing01", "ing02", "ing03"] },
                                 { "title": "recipe 02", "ingredients": ["ing01", "ing02", "ing03", "ing04" ] },
                                 { "title": "recipe 03", "ingredients": ["ing01", "ing02", "ing03", "ing06" ] },
                                 { "title": "recipe 04", "ingredients": ["ing01", "ing02", "ing03", "ing05" ] },
                                 { "title": "recipe 05", "ingredients": ["ing01", "ing02", "ing03", "ing07" ] }
    ] }';

        $this->ingredients = '{ "ingredients": [
            { "title": "ing01", "best-before": "2018-03-25", "use-by": "2018-03-27"},
            { "title": "ing02", "best-before": "2018-03-20", "use-by": "2018-03-27"},
            { "title": "ing03", "best-before": "2018-03-05", "use-by": "2018-03-27" },
            { "title": "ing04", "best-before": "2018-02-10", "use-by": "2018-02-27" },
            { "title": "ing06", "best-before": "2018-02-20", "use-by": "2018-02-25" },
            { "title": "ing07", "best-before": "2018-03-01", "use-by": "2018-03-02" }
        ]}';

        $this->recipesRepository = new RecipesRepository($this->recipes);
        $this->ingredientsRepository = new IngredientsRepository($this->ingredients);
        $this->lunch = new Lunch($this->recipesRepository, $this->ingredientsRepository);
    }

    /**
     * @test
     */
    public function it_return_only_recipes_that_has_availible_ingredients()
    {
        $date = '2018-02-19';
        $recipes = $this->lunch->getLunch($date);
        $this->assertCount(4, $recipes);
        // get the titles of the recipes
        $titles = array_map(function($lunch) {
            return $lunch['title'];
        }, $recipes);

        $this->assertFalse(in_array("recipe 04", $titles));
    }

    /**
     * @test
     */
    public function it_doesnt_return_recipes_with_ingredients_past_used_by()
    {
        $date = '2018-03-19';
        $recipes = $this->lunch->getLunch($date);

        $this->assertCount(1, $recipes);
        $this->assertEquals("recipe 01", $recipes[0]['title']);
    }

    /**
     * @test
     */
    public function it_return_recipes_past_best_by_date_at_the_end()
    {
        $date = '2018-02-22';
        // we should receive recipe 02 and  recipe 03 in the end of the list
        $recipes = $this->lunch->getLunch($date);

        $this->assertCount(4, $recipes);
        $this->assertEquals("recipe 02", $recipes[2]['title']);
        $this->assertEquals("recipe 03", $recipes[3]['title']);
    }
}
