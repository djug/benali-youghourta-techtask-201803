<?php
namespace app;

use App\IngredientsRepository;
use App\RecipesRepository;
use App\NotFoundException;
use DateTime;

class Lunch
{
    private $recipesRepository;
    private $ingredientsRepository;

    private $date;

    public function __construct(RecipesRepository $recipesRepository, IngredientsRepository $ingredientsRepository)
    {
        $this->recipesRepository = $recipesRepository;
        $this->ingredientsRepository = $ingredientsRepository;
    }

    public function getLunch($date = null)
    {
        if (! $date) {
            $date = date("Y-m-d"); //today
        }
        $this->date = $date;
        $allRecipes = $this->recipesRepository->getAll();

        // get all the recipes
        $allRecipesWithDates = array_map( [$this, 'addDates'], $allRecipes);
        // get just the recipes that we can prepare (filter out those with unavailable ingredients)
        $doableRecipes = array_filter($allRecipesWithDates, [$this, 'doableRecipes']);
        // get just the recipes that are in the "best-by" period
        $recommendedReciepes = array_filter($doableRecipes, [$this, 'freshest']);
        // get the recipes that are after the "best-by" date
        $notSoFreshReciepes = array_filter($doableRecipes, [$this, 'notSoFresh']);
        // put the "not so fresh" recipes (those after the "best-by" date) at the end of the list
        $allRecipes = array_merge($recommendedReciepes, $notSoFreshReciepes);
        // clean the list since we added dates to the recipes
        $cleanedRecipes = array_map([$this, 'clean'], $allRecipes);

        return $cleanedRecipes;

    }
    /**
     * Add the dates to a recipe so we can compare it to other recipe
     */
    public function addDates($recipe)
    {
        $bestBefore =  null;
        $useBy = null;

        $ingredients = $recipe['ingredients'];
        foreach ($ingredients as $ingredient) {
            try {
                $ingredientInfo = $this->ingredientsRepository->getByTitle($ingredient);

                $bestBefore = $this->earliestDate($bestBefore, $ingredientInfo['best-before']);
                $useBy = $this->earliestDate($useBy, $ingredientInfo['use-by']);
            } catch (NotFoundException $e) {
                // if the ingredient is not found, we will just set the recipe date to... the beginning of time ;)
                $date = new DateTime();
                $date->setTimestamp(0);
                $recipe['best-before'] = $date->format("Y-m-d");
                $recipe['use-by'] = $date->format("Y-m-d");

                return $recipe;
            }

        }
        $recipe['best-before'] = $bestBefore;
        $recipe['use-by'] = $useBy;

        return $recipe;
    }

    /**
     * a recipe is doable if all its ingredient are in their before used-by periods
     */
    public function doableRecipes($recipe)
    {
        if ($recipe['use-by'] < $this->date) {
            return false;
        }

        return true;
    }

    /**
     * a recipe is considered fresh if all its ingredient are in their best-before periods
     */
    public function freshest($recipe)
    {
        if ($recipe['best-before'] < $this->date) {
            return false;
        }

        return true;
    }

    /**
     * the opposite of fresh (i.e at least one ingredient is not in the "best-before" period )
     */
    public function notSoFresh($recipe)
    {
        if ($recipe['best-before'] >= $this->date) {
            return false;
        }

        return true;
    }

    /**
     * clean the additional dates we added to the recipes
     */
    public function clean($recipe)
    {
        unset($recipe['best-before']);
        unset($recipe['use-by']);

        return $recipe;
    }

    /**
     * comparing two dates (including when the first date is not set)
     */
    private function earliestDate($date1, $date2)
    {
        if (! $date1) {
            return $date2;
        }

        return strtotime($date1) < strtotime($date2) ? $date1 : $date2;
    }
}


