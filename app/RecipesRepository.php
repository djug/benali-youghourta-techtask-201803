<?php
namespace App;

class RecipesRepository extends JsonFileRepository
{
    protected $sourceFile = "recipes.json";
}
