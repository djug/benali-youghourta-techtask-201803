<?php
namespace app;

use Abraham\TwitterOAuth\TwitterOAuth;

class Twitter
{

    private $twitter;



    public function __construct()
    {
        $this->twitter = new TwitterOAuth(env('TWITTER_CONSUMER_KEY'), env('TWITTER_CONSUMER_SECRET'), env('APP_OAUTH_TOKEN'), env('APP_OAUTH_TOKEN_SECRET'));
    }

    public function tweet($text)
    {
        $parameters = ['status' => $text];
        $res = $this->twitter->post("statuses/update", $parameters);

        return $res;
    }
}
