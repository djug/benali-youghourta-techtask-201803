<?php
namespace App;

abstract class JsonFileRepository implements BasicRepository
{
    protected $sourceFile;

    protected $data;

    public function __construct($data = null)
    {
        $this->setData($data);
    }

    private function setData($data = null)
    {

        if (! $data) {
            try {
                $data = file_get_contents($this->sourceFile);
            } catch (\ErrorException $e) {
                dd("Could not find the following file: ". $this->sourceFile);
            }

        }

        $dataArray = json_decode($data, true);
        $this->data = array_shift($dataArray);

    }

    public function getAll()
    {
        return $this->data;
    }

    public function getByIndex($index)
    {
        if (isset($this->data[$index])) {

            return $this->data[$index];
        }
        throw new NotFoundException("Not Found");
    }

    public function getByTitle($title)
    {
        $res = array_filter( $this->data, function($element) use ($title) {

            if($element['title'] == $title) {

                return true;
            }

            return false;
        } );

        if ($res) {
            return array_shift($res);
        }
        throw new NotFoundException("Not Found");

    }



}
