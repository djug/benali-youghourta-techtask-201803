<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lunch;
use App\Twitter;



class LunchController extends Controller
{
    private $lunch;

    public function __construct(Lunch $lunch, Twitter $twitter)
    {
        $this->lunch = $lunch;
        $this->twitter = $twitter;
    }
    public function index()
    {
        $today =  $date = date("Y-m-d");
        $lunch = $this->lunch->getLunch($today);
        if (count ($lunch) > 0) {
            $first = $lunch[0];
            $status = 'I might eat today: '. $first['title'];
            $this->twitter->tweet($status);
            return $lunch;
        } else {
            return "No <strike> soup </strike> lunch for you";
        }
    }
}
