<?php
namespace App;

interface BasicRepository
{
    public function getAll();
    public function getByIndex(int $i);
    public function getByTitle(string $title);
}
